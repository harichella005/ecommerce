import './App.css';
import React from "react"
import Home from "./pages/home"
import About from "./pages/about"
import Login from "./pages/login"
import Blog from "./pages/blog"
import { Routes, BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/blog" element={<Blog />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;

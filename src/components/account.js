import React, { useState } from "react"
import "../css/account.css"
import { HiUserCircle } from 'react-icons/hi'
/*creating functional component for header*/

const Account = () => {
    const [ toggle, setToggle] = useState(false)
    return (
        <>
            <div className="header">
                    <button className="toggleuser" onClick={( )=> setToggle((prev) => !prev)}> 
                        {toggle ? (
                            <MdClose style={{ width: '32px', height: '32px' }} />
                            ) : (
                            <HiUserCircle
                                style={{
                                    width: '32px',
                                    height: '32px',
                                }}
                            />
                        )}
                    </button>
                    <ul className={`menu-nav${toggle ? ' show-menu' : ''}`}>
                        <li>
                            <a href="https://google.com" taget="_top">Home</a>
                        </li>
                        <li>
                            <a href="https://google.com">About</a>
                        </li>
                        <li>
                            <a href="https://google.com">Sarees</a>
                        </li>
                    </ul>
            </div>
        </>
    )
};

export default Account
import React, { useState } from "react"
import "../css/header.css"
import { MdClose } from 'react-icons/md'
import { FiMenu } from 'react-icons/fi'
import { HiUserCircle } from 'react-icons/hi'
import { HiShoppingCart } from 'react-icons/hi'
import { HiHeart } from 'react-icons/hi'
/*creating functional component for header*/

const Header = () => {
    const [ toggle, setToggle] = useState(false);
    function testing() {
        //alert(document.domain);
        
    }
    return (
        <>
            <div className="header">
                <h1 className="logo">
                    <a href="https://google.com" target="_self">Logo</a>
                </h1>
                <nav className="navbar">
                    <button className="toggle" onClick={( )=> setToggle((prev) => !prev)}> 
                        {toggle ? (
                            <MdClose style={{ width: '32px', height: '32px' }} />
                            ) : (
                            <FiMenu
                                style={{
                                    width: '32px',
                                    height: '32px',
                                }}
                            />
                        )}
                    </button>
                    <ul className={`menu-nav${toggle ? ' show-menu' : ''}`}>
                        <li>
                            <a href="/" taget="_top">Home</a>
                        </li>
                        <li>
                            <a href="/about">About</a>
                        </li>
                        <li>
                            <a href="https://google.com">Sarees</a>
                        </li>
                        <li>
                            <a href="https://google.com" taget="_top">Home</a>
                        </li>
                        <li>
                            <a href="https://google.com">About</a>
                        </li>
                        <li>
                            <a href="https://google.com">Sarees</a>
                        </li>
                    </ul>
                    {/* UserIcon button code */}
                    <button className="toggle1" onClick={( )=> setToggle((prev) => !prev)}> 
                        {toggle ? (
                            <HiUserCircle style={{ width: '32px', height: '32px' }} />
                            ) : (
                            <HiUserCircle
                                style={{
                                    width: '32px',
                                    height: '32px',
                                }}
                            />
                        )}
                    </button>
                    <ul className={`menu-nav1${toggle ? ' show-menu1' : ''}`}>
                        <li>
                            <a href="https://google.com" taget="_top">Profile</a>
                        </li>
                        <li>
                            <a href="https://google.com">Account</a>
                        </li>
                        <li>
                            <a href="https://google.com">Sarees</a>
                        </li>
                    </ul>
                    <button className="cart">
                            <HiShoppingCart onClick={testing}
                                style={{
                                    width: '32px',
                                    height: '32px',
                                }}
                            />
                    </button>
                    <button className="favourite">
                            <HiHeart onClick={testing}
                                style={{
                                    width: '32px',
                                    height: '32px',
                                }}
                            />
                    </button>
                    
                </nav>

                
            </div>
        </>
    )
};

export default Header